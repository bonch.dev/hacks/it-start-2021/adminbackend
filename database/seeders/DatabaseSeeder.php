<?php

namespace Database\Seeders;

use App\Models\Device;
use App\Models\DeviceLog;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Device::factory()->create();

        DeviceLog::factory()->count(100)->create();
    }
}
