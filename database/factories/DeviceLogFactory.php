<?php

namespace Database\Factories;

use App\Models\Device;
use App\Models\DeviceLog;
use Illuminate\Database\Eloquent\Factories\Factory;

class DeviceLogFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = DeviceLog::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $now = now()->subDays(random_int(1,90))->subSeconds(random_int(1,99));

        return [
            "device_id" => Device::inRandomOrder()->first()->id,
            "pressure" => random_int(0, 1023),
            "battery" => $this->faker->randomFloat(2, 0, 1),
            "is_critical" => $this->faker->boolean(80),
            "created_at" => $now,
            "updated_at" => $now
        ];
    }

    public function critical()
    {
        return $this->state(function () {
            return [
                "is_critical" => true
            ];
        });
    }
}
