<?php

namespace Database\Factories;

use App\Models\Device;
use App\Models\DeviceAlert;
use Illuminate\Database\Eloquent\Factories\Factory;

class DeviceAlertFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = DeviceAlert::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $now = now()->subDays(random_int(1,90))->subSeconds(random_int(1,99));

        return [
            "device_id" => Device::inRandomOrder()->first()->id,
            "pressure" => random_int(0, 1023),
            "delta" => [10, 40, 50, 30, 20, 40, 30, 14, 39, 23],
            "battery" => $this->faker->randomFloat(2, 0, 1),
            "created_at" => $now,
            "updated_at" => $now
        ];
    }
}
