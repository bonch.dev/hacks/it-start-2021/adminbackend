<?php

use App\Models\Device;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeviceAlertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('device_alerts', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Device::class);
            $table->integer("pressure");
            $table->decimal("battery");
            $table->boolean("is_critical")->default(true);
            $table->jsonb("delta")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('device_alerts');
    }
}
