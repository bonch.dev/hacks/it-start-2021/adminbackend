<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class HealthCheckServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->routes(function () {
            Route::get("healthcheck", function () {
                try {
                    // you may add your check

                    // check DB connection
                    DB::connection()->getPdo();

                    // uncomment if need check redis connection
                    // Redis::connection()->getName();
                } catch (Exception $exception) {
                    return response("not ok", 500);
                }

                return response("ok");
            })->name("healthcheck");
        });
    }
}