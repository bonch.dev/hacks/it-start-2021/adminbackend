<?php

namespace App\Http\Controllers;

use App\Http\Requests\Device\DeviceAlertRequest;
use App\Http\Requests\Device\DeviceInfoRequest;
use App\Models\Device;
use Illuminate\Http\Request;

class DeviceController extends Controller
{
    public function info(DeviceInfoRequest $request, Device $device)
    {
        \Log::info("
            Device {$device->id}\n
            ===\n
            Pressure: {$request->pressure}\n
            Battery: {$request->battery}\n
        ");

        $device->logs()->create(
            $request->validated()
        );

        return response("ok");
    }

    public function alert(DeviceAlertRequest $request, Device $device)
    {
        \Log::alert("
            Device {$device->id}\n
            ===\n
            Pressure: {$request->pressure}\n
            Battery: {$request->battery}\n
        ");

        $data = $request->validated();

        $device->alerts()->create(
            $data
        );

        return response("ok");
    }
}
