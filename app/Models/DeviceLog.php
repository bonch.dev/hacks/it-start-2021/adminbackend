<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Metrics\Chartable;
use Orchid\Screen\AsSource;

class DeviceLog extends Model
{
    use HasFactory;
    use AsSource;
    use Chartable;

    protected $fillable = [
        "pressure",
        "battery",
        "is_critical"
    ];
}
