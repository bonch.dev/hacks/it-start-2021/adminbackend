<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\AsSource;

class Device extends Model
{
    use HasFactory;
    use AsSource;

    protected $fillable = [
        "storage_tank"
    ];

    public function getLatestLogAttribute()
    {
        return $this->logs()->latest()->first();
    }

    public function alerts()
    {
        return $this->hasMany(DeviceAlert::class);
    }

    public function logs()
    {
        return $this->hasMany(DeviceLog::class);
    }
}
