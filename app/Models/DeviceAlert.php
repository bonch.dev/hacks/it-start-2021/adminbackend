<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\AsSource;

class DeviceAlert extends Model
{
    use HasFactory;
    use AsSource;

    protected $fillable = [
        "delta",
        "pressure",
        "battery"
    ];

    protected $casts = [
        "delta" => "array"
    ];

    public function device()
    {
        return $this->belongsTo(Device::class);
    }
}
