<?php

namespace App\Orchid\Screens\Device;

use App\Models\Device;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Screen\TD;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;

class DeviceListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Список устройств';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = 'Отображение устройств, расположенные в резервуарах';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            "devices" => Device::paginate()
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make("Добавить устройство")
                ->icon("plus")
                ->type(Color::PRIMARY())
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::table("devices", [
                TD::make("id", "ID"),
                TD::make("storage_tank", "Номер резервуара"),
                TD::make("actions", "Действия")
                    ->width(20)
                    ->render(function ($model) {
                        return Link::make("")
                            ->icon("eye")
                            ->route("platform.devices.show", [
                                "device" => $model->id
                            ]);
                    })
            ])
        ];
    }
}
