<?php

namespace App\Orchid\Screens\Device;

use App\Models\Device;
use App\Orchid\Layouts\Device\DeviceBatteryChart;
use App\Orchid\Layouts\Device\DevicePressureChart;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Label;
use Orchid\Screen\Screen;
use Orchid\Screen\TD;
use Orchid\Support\Facades\Layout;

class DeviceShowScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Устройство';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = 'История показаний давления и заряда аккумулятора';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Device $device): array
    {
        $this->name = "Устройство \"{$device->id}\"";

        $logs = $device->logs()
            ->latest()
            ->take(10)
            ->get();

        $data = $logs->map(function($log) {
            return [
                "created_at" => $log->created_at->toDateTimeString(),
                "pressure" => $log->pressure,
                "battery" => $log->battery,
                "is_critical" => $log->is_critical,
                "color" => !$log->is_critical
                    ? '#00CC66' // green
                    : '#D90368' // red
            ];
        });

        return [
            "device" => $device,
            "alerts" => $device->alerts()->paginate(),
            "logs" => $logs,
            "pressure" => [
                [
                    'labels' => $data->pluck("created_at"),
                    'title'  => 'Значения',
                    'values' => $data->pluck("pressure"),
                ],
            ],
            "battery" => [
                [
                    'labels' => $logs
                        ->pluck("created_at")
                        ->map(function ($createdAt) {
                            return $createdAt->isoFormat('dddd, MMMM Do YYYY, h:mm:ss');
                        })
                        ->toArray(),
                    'title'  => 'Заряд батареи',
                    'values' => $logs->pluck("battery")->toArray(),
                ]
            ]
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::columns([
                Layout::rows([
                    Label::make("device.id")
                        ->title("ID устройства"),
                    Label::make("device.storage_tank")
                        ->title("Резервуар"),
                ])->title("Основная информация"),
                Layout::table("alerts", [
                    TD::make("pressure", "Давление"),
                    TD::make("battery", "Заряд батареи"),
                    TD::make("created_at", "Дата")
                        ->render(function ($model) {
                            return $model->created_at->toDateTimeString();
                        }),
                    TD::make("actions", "Действия")
                        ->render(function ($model) {
                            return Link::make("")
                                ->route("platform.alerts.show", [
                                    "alert" => $model->id
                                ])
                                ->icon("eye");
                        }),
                ])->title("Критические значения")
            ]),
            DevicePressureChart::class,
            DeviceBatteryChart::class,
            Layout::columns([
                Layout::table("logs", [
                    TD::make("pressure", "Давление"),
                    TD::make("is_critical", "Критическое значение")
                ]),
                Layout::table("logs", [
                    TD::make("battery", "Заряд батареи"),
                    TD::make("is_critical", "Критическое значение")
                ]),
            ])
        ];
    }
}
