<?php

namespace App\Orchid\Screens\Device;

use App\Models\DeviceAlert;
use App\Orchid\Layouts\Device\DeviceAlertDeltaChart;
use Orchid\Screen\Fields\Label;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;

class DeviceAlertShow extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Критические показатели';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = null;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(DeviceAlert $alert): array
    {
        $this->description = "
            Критические показания устройства \"{$alert->device->id}\"
            резервуара {$alert->device->storage_tank}
        ";

        return [
            "alert" => $alert,
            "device" => $alert->device,
            "delta" => [
                [
                    "values" => $alert->delta,
                    "labels" => ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
                    "title" => "Дельта изменений"
                ]
            ]
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::columns([
                Layout::rows([
                    Label::make("device.id")
                        ->title("Номер устройства"),
                    Label::make("device.storage_tank")
                        ->title("Резервуар"),
                ])->title("Информация об устройстве"),
                Layout::rows([
                    Label::make("alert.pressure")
                        ->title("Давление"),
                    Label::make("alert.battery")
                        ->title("Заряд батареи")
                ])->title("Показания"),
            ]),
            DeviceAlertDeltaChart::class
        ];
    }
}
