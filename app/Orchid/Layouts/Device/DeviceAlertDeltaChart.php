<?php

namespace App\Orchid\Layouts\Device;

use Orchid\Screen\Layouts\Chart;

class DeviceAlertDeltaChart extends Chart
{
    /**
     * Add a title to the Chart.
     *
     * @var string
     */
    protected $title = 'Дельта';

    /**
     * Available options:
     * 'bar', 'line',
     * 'pie', 'percentage'.
     *
     * @var string
     */
    protected $type = 'line';

    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the chart.
     *
     * @var string
     */
    protected $target = 'delta';

    /**
     * Determines whether to display the export button.
     *
     * @var bool
     */
    protected $export = false;
}
