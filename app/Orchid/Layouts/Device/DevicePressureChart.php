<?php

namespace App\Orchid\Layouts\Device;

use Orchid\Screen\Layouts\Chart;

class DevicePressureChart extends Chart
{
    /**
     * Configuring line.
     *
     * @var array
     */
    protected $lineOptions = [
        'regionFill' => 0,
        'hideDots'   => 0,
        'hideLine'   => 0,
        'heatline'   => 0,
        'dotSize'    => 4,
        'spline'     => 0,
    ];

    /**
     * @var int
     */
    protected $height = 400;

    /**
     * Colors used.
     *
     * @var array
     */
    protected $colors = [
        '#00CC66', // green
        '#D90368', // red
    ];

    /**
     * Add a title to the Chart.
     *
     * @var string
     */
    protected $title = 'Давление';

    /**
     * Available options:
     * 'bar', 'line',
     * 'pie', 'percentage'.
     *
     * @var string
     */
    protected $type = 'line';

    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the chart.
     *
     * @var string
     */
    protected $target = 'pressure';

    /**
     * Determines whether to display the export button.
     *
     * @var bool
     */
    protected $export = false;
}
