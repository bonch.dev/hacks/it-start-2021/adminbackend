<?php

namespace App\Orchid\Layouts\Device;

use Orchid\Screen\Layouts\Chart;

class DeviceBatteryChart extends Chart
{
    protected function markers(): ?array
    {
        return [
            [
                "label" => "Критический порог",
                "value" => 0.1,
                "options" => [
                    "labelPos" => "left"
                ]
            ]
        ];
    }

    /**
     * @var int
     */
    protected $height = 300;

    /**
     * Add a title to the Chart.
     *
     * @var string
     */
    protected $title = 'Заряд батареи';

    /**
     * Available options:
     * 'bar', 'line',
     * 'pie', 'percentage'.
     *
     * @var string
     */
    protected $type = 'bar';

    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the chart.
     *
     * @var string
     */
    protected $target = 'battery';

    /**
     * Determines whether to display the export button.
     *
     * @var bool
     */
    protected $export = false;
}
