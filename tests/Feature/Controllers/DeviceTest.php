<?php

namespace Tests\Feature\Controllers;

use App\Models\Device;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DeviceTest extends TestCase
{
    public function test_info()
    {
        $device = Device::factory()->create();

        $response = $this->postJson(
            route("devices.info", [
                "device" => $device->id
            ]),
            [
                "pressure" => 1,
                "battery" => 1
            ]
        );

        $response
            ->dump()
            ->assertStatus(200);
    }

    public function test_alert()
    {
        $device = Device::factory()->create();

        $response = $this->postJson(
            route("devices.alert", [
                "device" => $device->id
            ]),
            [
                "pressure" => 1,
                "battery" => 1
            ]
        );

        $response
            ->dump()
            ->assertStatus(200);
    }
}
